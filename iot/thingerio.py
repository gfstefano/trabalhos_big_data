import requests
import pdb
import time
import json
import datetime
import pymongo
from bson import json_util, ObjectId

import asyncio
import aiohttp
from aiohttp import web
from aiohttp_sse import sse_response
import aiohttp_cors

# ####################################################################################
# Conexao com o banco
# ####################################################################################
conexao = pymongo.MongoClient("mongodb+srv://alunos:projetointegrador@cluster0-xir76.mongodb.net/test?retryWrites=true")
mydb = conexao['gilberto']

dados = []
dados_banco = mydb.aula1.find()
print(dados_banco[0])

url_water = 'https://api.thinger.io/v2/users/gavatales/devices/projetoiot/water'
url_humidity = 'https://api.thinger.io/v2/users/gavatales/devices/projetoiot/humidity'
url_temperature = 'https://api.thinger.io/v2/users/gavatales/devices/projetoiot/temperature'

headers = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXYiOiJwcm9qZXRvaW90IiwiaWF0IjoxNTU5MTU1NzIwLCJqdGkiOiI1Y2VlZDQwODU5N2JkM2E3YjlhYWIyOTgiLCJ1c3IiOiJnYXZhdGFsZXMifQ.1QN2unySL_-IksVUf1fyXOALynwfCsWlvJDN4ZWl3A4',
    'Accept': 'application/json, text/plain, /',
}


while True:

    try:
        r_water = requests.get(url_water, headers=headers)
        r_humidity = requests.get(url_humidity, headers=headers)
        r_temperature = requests.get(url_temperature, headers=headers)

        print(r_water.content.decode())
        print(r_humidity.content.decode())
        print(r_temperature.content.decode())

        insert_water = {
            'name': 'ground',
            'date': datetime.datetime.now(),
            'out': json.loads(r_water.content.decode())['out']
        }

        insert_humidity = {
            'name': 'humidity',
            'date': datetime.datetime.now(),
            'out': json.loads(r_humidity.content.decode())['out']
        }

        insert_temperature = {
            'name': 'temperature',
            'date': datetime.datetime.now(),
            'out': json.loads(r_temperature.content.decode())['out']
        }

        mydb.aula1.insert_one(insert_water).inserted_id
        mydb.aula1.insert_one(insert_humidity).inserted_id
        mydb.aula1.insert_one(insert_temperature).inserted_id
    
    except:
        print("Deu erro")

    time.sleep(300)