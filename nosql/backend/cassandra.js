const cassandra = require('cassandra-driver');
const async = require('async');
const assert = require('assert');
const express = require('express');
var dateTime = require('node-datetime');

// Create a YB CQL client.
// DataStax Nodejs 4.0 loadbalancing default is TokenAwarePolicy with child DCAwareRoundRobinPolicy
// Need to provide localDataCenter option below or switch to RoundRobinPolicy
const loadBalancingPolicy = new cassandra.policies.loadBalancing.RoundRobinPolicy ();
const client = new cassandra.Client({ contactPoints: ['localhost:9042'], policies : { loadBalancing : loadBalancingPolicy }});

const app = express();
app.use(express.json());
app.use(express.urlencoded());

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	//intercepts OPTIONS method
	if ('OPTIONS' === req.method) {
		//respond with 200
		res.sendStatus(200);
	}
	else {
		//move on
		next();
	}
});

app.get('/', function (req, res) {
    client.connect();
})

app.get('/create-table', function (req, res) {	
    const create_table = 'CREATE TABLE IF NOT EXISTS nosql.teste (id int PRIMARY KEY, ' + 'name varchar, ' + 'age int, ' + 'language varchar);';
    console.log('Creating table employee');
    client.execute(create_table);
});

app.get('/list-produtos', function (req, res) {
  console.log('List produtos')
  const list_produtos = 'SELECT * FROM nosql.produtos';
  client.execute(list_produtos, function(err, result){
    if (err) {
      console.log(err)
    }
    else {
      res.send(result.rows)
    }
  });
});

app.get('/create-produto', function (req , res) {
  var body = req.query
  
  console.log('Criar produto')
  console.log('Criar user', body)
  const create_user = "INSERT INTO nosql.produtos (id, nome, preco, descricao) " +
  "VALUES (?, ?, ?, ?); ";

  date = dateTime.create();
  client.execute(create_user, [date.now(), body.nome, parseFloat(body.preco), body.descricao], {prepare: true}, function(err, result){
    if (err) {
      console.log('Erro', err)
    }
    else {
      console.log('Criando produto')
    }
  });
});

app.delete('/delete-produto', function (req , res) {
  var body = req.query
  var params = [body.id]

  console.log('Deletar produto', req.query.id)
  const delete_produto = "DELETE FROM nosql.produtos WHERE id = "+ body.id +"";

  client.execute(delete_produto);
});

app.get('/list-users', function (req, res) {
  console.log('List users')
  const list_users = 'SELECT * FROM nosql.clientes';
  client.execute(list_users, function(err, result){
    if (err) {
      console.log(err)
    }
    else {
      res.send(result.rows)
    }
  });
});


app.delete('/delete-user', function (req , res) {
  var body = req.query
  var params = [body.id]

  console.log('Deletar usuario', req.query.id)
  const delete_user = "DELETE FROM nosql.clientes WHERE id = "+ body.id +"";

  client.execute(delete_user);
});

app.get('/create-user', function (req , res) {
  var body = req.query
  var params = [body.nome, body.endereco, body.email, body.senha]

  console.log('Criar user', body)
  const create_user = "INSERT INTO nosql.clientes (id, nome, endereco, email, senha) " +
  "VALUES (?, ?, ?, ?, ?); ";

  date = dateTime.create();
  client.execute(create_user, [date.now(), body.nome, body.endereco, body.email, body.senha], {prepare: true}, function(err, result){
    if (err) {
      console.log('Erro', err)
    }
    else {
      console.log('Criando usuario')
    }
  });
});

app.get('/create-pedido', function (req , res) {
  const body = req.query
  console.log(body.cliente_id)

  const search_user = 'SELECT * FROM nosql.clientes WHERE id = '+ body.cliente_id +' ALLOW FILTERING';

  client.execute(search_user, function(err, result) {
    if (err) {
      console.log('===== DEU ERRO =====', err)
    }
    else {
      console.log('===== TESTE =====', body.produto_preco)

      let body2 = {
      'data': body.data, 
      'valor': parseFloat(body.valor), 
      'pagamento': body.pagamento,
        'produtos': { 
          'id': body.produto_id,
          'nome': body.produto_nome,
          'preco': parseFloat(body.produto_preco),
          'descricao': body.produto_descricao
        },
        'clientes': {
          'id': result.rows[0].id,
          'nome': result.rows[0].nome,
          'endereco': result.rows[0].endereco,
          'email': result.rows[0].email,
          'senha': result.rows[0].senha
        }
      }

      date = dateTime.create();
      const create_pedido = "INSERT INTO nosql.pedidos (id, data, valor, pagamento, produtos, clientes) " +
      "VALUES (?, ?, ?, ?, {id: ?, nome: ?, preco: ?, descricao: ?}, {id: ?, nome: ?, endereco: ?, email: ?, senha: ?});";
      client.execute(create_pedido, [date.now(), body2.data, body2.valor, body2.pagamento, body2.produtos.id, body2.produtos.nome, body2.produtos.preco, body2.produtos.descricao, body2.clientes.id, body2.clientes.nome, body2.clientes.endereco, body2.clientes.email, body2.clientes.senha], {prepare: true}, function(err, result) {
        if (err) {
          console.log("Infelizmente deu erro", err)
        }
        else {
          console.log('Pedido realizado')
        }
      })
    }
  });
  
});

app.get('/search-user', function (req , res) {
  var body = req.query

  console.log('Search user', body)
  const search_user = 'SELECT * FROM nosql.clientes WHERE email = ? AND senha = ? ALLOW FILTERING';

  client.execute(search_user, [body.username, body.password], function(err, result) {
    if (err) {
      console.log(err)
    }
    else {
      res.send(result.rows)
    }
  });
});

app.get('/list-pedidos', function (req, res) {
  console.log('List pedidos')
  const list_pedidos = 'SELECT * FROM nosql.pedidos';
  client.execute(list_pedidos, function(err, result){
    if (err) {
      console.log(err)
    }
    else {
      res.send(result.rows)
    }
  });
});

app.delete('/delete-pedido', function (req , res) {
  var body = req.query
  var params = [body.id]

  console.log('Deletar pedido', req.query.id)
  const delete_pedido = "DELETE FROM nosql.pedidos WHERE id = "+ body.id +"";

  client.execute(delete_pedido);
});



console.log('haahahhah')

async.series([
  function connect(next) {
    client.connect(next);
  },
//   function createKeyspace(next) {
//     console.log('Creating keyspace ybdemo');
//     client.execute('CREATE KEYSPACE IF NOT EXISTS ybdemo;', next);
//   },
  function createTableProdutos(next) {
    const create_table = 'CREATE TABLE IF NOT EXISTS nosql.produtos (id bigint PRIMARY KEY, ' + 'nome varchar, ' + 'preco double, ' + 'descricao varchar);';
    console.log('Creating table Produtos');
    client.execute(create_table, next);
  },
  function insertProdutos(next) {
    const insert = "INSERT INTO nosql.produtos (id, nome, preco, descricao) " +
                                        "VALUES (1, 'Celular', 2.900, 'O melhor celular que existe');";
    const insert2 = "INSERT INTO nosql.produtos (id, nome, preco, descricao) " +
                                        "VALUES (3, 'Garrafa', 10, 'Garrafa de 2L');";
    console.log('Inserting row with: %s', insert)
    client.execute(insert);1, 'Celular', 2.900, 'O melhor celular que existe'
    client.execute(insert2, next);
  },

  function createTableClientes(next) {
    const create_table = 'CREATE TABLE IF NOT EXISTS nosql.clientes (id bigint PRIMARY KEY, ' + 'nome varchar, ' + 'endereco varchar, ' + 'email varchar, ' + 'senha varchar);';
    console.log('Creating table Clientes');
    client.execute(create_table, next);
  },
  function insert(next) {
    const insert = "INSERT INTO nosql.clientes (id, nome, endereco, email, senha) " +
                                        "VALUES (2, 'Filipe', 'Jamil dib lutf 165', 'gilbertofilipe', '1234');";
    console.log('Inserting row with: %s', insert)
    client.execute(insert, next);
  },

  function crateTypeProdutos(next) {
    const create_type_produtos = 'CREATE TYPE IF NOT EXISTS nosql.produtos (id bigint, nome varchar, preco double, descricao varchar);';
    console.log('Create type produtos');
    client.execute(create_type_produtos, next);
  },

  function crateTypeClientes(next) {
    const create_type_clientes = 'CREATE TYPE IF NOT EXISTS nosql.clientes (id bigint, nome varchar, endereco varchar, email varchar, senha varchar);';
    console.log('Create type clientes');
    client.execute(create_type_clientes, next);
  },

  function createPedido(next) {
    const create_pedido = 'CREATE TABLE IF NOT EXISTS nosql.pedidos (id bigint PRIMARY KEY, ' + 'data text, ' + 'valor double, ' + 'pagamento varchar, ' + 'produtos FROZEN<produtos>, ' + 'clientes FROZEN<clientes>);';
    console.log('Inserting row with: %s', create_pedido)
    client.execute(create_pedido, next);
  },

  function insert(next) {
    date = dateTime.create();
    const insert = "INSERT INTO nosql.pedidos (id, data, valor, pagamento, produtos, clientes) " +
                                        "VALUES ("+date.now()+", '07/05/2001', 25, 'cartao', {id: 1, nome: 'Celular', preco: 2.900, descricao: 'O melhor celular que existe'}, {id: 1, nome: 'Filipe', endereco: 'Jamil dib lutf 165', email: 'gilbertofilipe@outlook.com', senha: '1234'});";
    console.log('Inserting row with: %s', insert)
    client.execute(insert, next);
  },

  // function select(next) {
  //   const select = 'SELECT name, age, language FROM nosql.employee WHERE id = 1;';
  //   client.execute(select, function (err, result) {
  //     if (err) return next(err);
  //     var row = result.first();
  //     console.log('Query for id=1 returned: name=%s, age=%d, language=%s',
  //                                           row.name, row.age, row.language);
  //     next();
  //   });
  // }

], function (err) {
  if (err) {
    console.error('Ocorreu algum erro', err.message, err.stack);
  }
  console.log('API rodando!');
//   client.shutdown();
});

app.listen(3000);
