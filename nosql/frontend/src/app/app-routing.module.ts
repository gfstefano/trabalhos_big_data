import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdutosComponent } from './produtos/produtos.component';
import { UsersComponent } from './users/users.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { LoginComponent } from './login/login.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { CadastroProdutoComponent } from './cadastro-produto/cadastro-produto.component';

const routes: Routes = [
  {
      path: '',
      component: ProdutosComponent,
  },
  {
      path: 'produtos',
      component: ProdutosComponent,
  },
  {
      path: 'users',
      component: UsersComponent,
  },
  {
    path: 'create-user',
    component: CadastroComponent,
  },
  {
    path: 'create-produtos',
    component: CadastroProdutoComponent,
  },
  {
      path: 'pedidos',
      component: PedidosComponent,
  },
  {
      path: 'login',
      component: LoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
