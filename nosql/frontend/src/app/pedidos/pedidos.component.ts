import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { PedidosDataSource } from './pedidos-datasource';
import { HttpClient } from '@angular/common/http';
import { CassandraService } from '../cassandra.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[]
  public dataSource;

  public pedidos: any = [];

  constructor(
    private service: CassandraService,
    private http: HttpClient,
  ) {
    this.pedidos = this.getPedidos()
  }
  
  ngAfterViewInit() {
  
  }

  getPedidos(){
    return this.service.getPedidos().subscribe(
      res => {
        console.log('res', res)
        this.pedidos = res
        this.displayedColumns = ['id', 'data', 'valor', 'pagamento', 'produtos.nome', 'clientes.nome', 'deletar'];
        this.dataSource = this.pedidos; 
      },
      err => console.log(err)
    );
  }

  deletePedido(id){
    return this.service.deletePedido(id).subscribe(
      res => console.log('DELETE PEDIDO'),
      err => console.log(err)
    )
  }

}
