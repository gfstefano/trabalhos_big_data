import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { UsersDataSource } from './users-datasource';
import { CassandraService } from '../cassandra.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[]
  public dataSource;

  public produtos: any = [];

  constructor(
    private service: CassandraService,
    private http: HttpClient,
  ) {
    this.produtos = this.getUsers()
  }

  ngAfterViewInit() {

  }

  getUsers(){
    return this.service.getUsers().subscribe(
      res => {
        console.log('res', res)
        this.produtos = res
        this.displayedColumns = ['id', 'nome', 'email', 'endereco', 'deletar'];
        this.dataSource = this.produtos; 
      },
      err => console.log(err)
    );
  }

  deleteUser(id){
    return this.service.deleteUser(id).subscribe(
      res => console.log('DELETE USER'),
      err => console.log(err)
    )
  }

}
