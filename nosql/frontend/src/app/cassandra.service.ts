import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CassandraService {

  private api = "http://localhost:3000/";

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { 

  }

  getProdutos() {
    console.log('Lista de produtos')
    return this.http.get(this.api+'list-produtos')
  }

  createUser(nome, endereco, email, senha) {
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
    let httpParams = new HttpParams()
                        .set('nome', nome)
                        .set('endereco', endereco)
                        .set('email', email)
                        .set('senha', senha)
                        
    console.log('Criar usuario', nome)
    return this.http.get(`${this.api}create-user`, {
      headers: httpHeaders,
      params: httpParams, 
      responseType: 'json'
    })
  }

  createProduto(nome, preco, descricao) {
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
    let httpParams = new HttpParams()
                        .set('nome', nome)
                        .set('preco', preco)
                        .set('descricao', descricao)
                        
    console.log('Criar produto', nome)
    return this.http.get(`${this.api}create-produto`, {
      headers: httpHeaders,
      params: httpParams, 
      responseType: 'json'
    })
  }

  createPedido(pedido) {
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
    let httpParams = new HttpParams()
                        .set('data', pedido.data)
                        .set('valor', pedido.valor)
                        .set('pagamento', pedido.pagamento)
                        .set('produto_id', pedido.produtos.id)
                        .set('produto_nome', pedido.produtos.nome)
                        .set('produto_preco', pedido.produtos.preco)
                        .set('produto_descricao', pedido.produtos.descricao)
                        .set('cliente_id', pedido.clientes.id);
    
    console.log('Deletar produto', pedido)
    return this.http.get(`${this.api}create-pedido`, {
      headers: httpHeaders,
      params: httpParams, 
      responseType: 'json'
    })
  }

  deleteProduto(id) {
  let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
  let httpParams = new HttpParams()
                        .set('id', id);
    
    console.log('Deletar produto', id)
    let body = { params: id }
    return this.http.delete(`${this.api}delete-produto`, {
      headers: httpHeaders,
      params: httpParams, 
      responseType: 'json'
    })
  }

  getUsers() {
    console.log('Listar usuários')
    return this.http.get(this.api+'list-users')
  }

  deleteUser(id) {
    let httpHeaders = new HttpHeaders()
                           .set('Accept', 'application/json');
    let httpParams = new HttpParams()
                          .set('id', id);
      
      console.log('Deletar usuario', id)
      let body = { params: id }
      return this.http.delete(`${this.api}delete-user`, {
        headers: httpHeaders,
        params: httpParams, 
        responseType: 'json'
      })
    }

    getPedidos() {
      console.log('Listar pedidos')
      return this.http.get(this.api+'list-pedidos')
    }
  
    deletePedido(id) {
      let httpHeaders = new HttpHeaders()
                             .set('Accept', 'application/json');
      let httpParams = new HttpParams()
                            .set('id', id);
        
        console.log('Deletar pedido', id)
        let body = { params: id }
        return this.http.delete(`${this.api}delete-pedido`, {
          headers: httpHeaders,
          params: httpParams, 
          responseType: 'json'
        })
      }


      login(username, password) {
        console.log('Login')
        let httpHeaders = new HttpHeaders()
                             .set('Accept', 'application/json');
        let httpParams = new HttpParams()
                              .set('username', username)
                              .set('password', password);
          
          return this.http.get(`${this.api}search-user`, {
            headers: httpHeaders,
            params: httpParams, 
            responseType: 'json'
          })
        
      }

}
