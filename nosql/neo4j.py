from neo4jrestclient.client import GraphDatabase
 
db = GraphDatabase("http://localhost:7474", username="neo4j", password="root")
 
# Create some nodes with labels
user = db.labels.create("User")
u1 = db.nodes.create(name="Marco")
user.add(u1)
u2 = db.nodes.create(name="Daniela")
user.add(u2)
u3 = db.nodes.create(name="Jessica")
user.add(u3)
u4 = db.nodes.create(name="Filipe")
user.add(u4)
u5 = db.nodes.create(name="Melissa")
user.add(u5)
 
ling = db.labels.create("Linguagens")
b1 = db.nodes.create(name="Java")
b2 = db.nodes.create(name="PHP")
b3 = db.nodes.create(name="Python")
b4 = db.nodes.create(name="JavaScript")
b5 = db.nodes.create(name="Go")
ling.add(b1, b2, b3, b4, b5)

u1.relationships.create("likes", b1)
u1.relationships.create("likes", b2)
u2.relationships.create("likes", b3)
u3.relationships.create("likes", b1)
u3.relationships.create("likes", b5)
u4.relationships.create("likes", b4)
u5.relationships.create("likes", b3)
u5.relationships.create("likes", b2)

