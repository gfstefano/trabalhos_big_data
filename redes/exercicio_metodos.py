


class comecar(object):
    # Inicio
    def __init__(self, valor):
        self.valor = valor

    # Funcoes
    def string(self, posicao):
        self.posicao = posicao
        print "String: ", (format(self.valor))
        return "Frase normal: {}".format(self.valor)
    
    def invert_string(self, posicao):
        self.posicao = posicao
        if(self.posicao == "baixo"):
            inverter = self.valor[::-1]
            print "Invert string baixo: ", (format(inverter))
            return "Frase invertida: {}".format(inverter)
        else:
            inverter = self.valor[::1]
            print "Invert string cima: ", (format(inverter))
            return inverter

    def separa_string(self, posicao):
        self.posicao = posicao
        print posicao
        if(self.posicao == "baixo"):
            separar = list(self.valor)
            return "String to list: {}".format(separar)
        else:
            Lista = list()
            Lista = self.posicao
            # print Lista
            # print "List to string:",("".join(str(x) for x in Lista))


# Iniciando o objeto
exemplo = comecar("Eu amo o Sao Paulo")
# exemplo_invert = comecar("Eu amo o Sao Paulo")

# string = list("Eu amo o Sao Paulo")
exemplo_list = comecar(['E', 'u', ' ', 'a', 'm', 'o', ' ', 'o', ' ', 'S', 'a', 'o', ' ', 'P', 'a', 'u', 'l', 'o'])
print comecar(['E', 'u', ' ', 'a', 'm', 'o', ' ', 'o', ' ', 'S', 'a', 'o', ' ', 'P', 'a', 'u', 'l', 'o'])

def baixo():
    baixo = "baixo"
    exemplo.invert_string(baixo)
    exemplo.string(baixo)
    exemplo.separa_string(baixo)

def cima():
    cima = "cima"
    exemplo.invert_string(cima)
    exemplo.string(cima)
    exemplo_list.separa_string(cima)

baixo()
cima()
        