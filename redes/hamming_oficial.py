def autocompletar(string):

    # Converter string em lista
	string = list(string)

	x = posicion = 0
	while posicion < len(string):
		posicion = 2 ** x

		if posicion < len(string):
			string.insert(posicion-1, "*")
		else:
			break
		x += 1

	string = "".join(string)
	return string


def calcularFila(stringAuto, salto, stringTemporal=""):
	originalStringAuto = stringAuto

	stringAuto = stringAuto[salto-1:]
	n = "N"*(salto-1)
	stringTemporal += n

	n = "N"*salto
	nsalto = salto * 2
	while len(stringAuto) > 0:
		stringTemporal += stringAuto[:salto]
		stringAuto = stringAuto[nsalto:]
		stringTemporal += n

	stringTemporal = stringTemporal[:len(originalStringAuto)]

	return stringTemporal


def obterFilas(stringAuto):
	filasDeParidad = dict()

	totalFilas = stringAuto.count("*")
	filaActual = 0

    # Temos uma fila para cada elemento
	while totalFilas > filaActual:
		salto = 2 ** filaActual
		filasDeParidad[salto] = calcularFila(stringAuto, salto)
		filaActual += 1
	return filasDeParidad


def buscarErros(filasDeParidad):
	filasErros = list()
	for chave, conteudo in filasDeParidad.items():
		sumatoria = 0
		for elemento in conteudo:
			for caracter in elemento:
				if caracter != "*" and caracter != "N":
					sumatoria += int(caracter)
		if sumatoria % 2 != 0:
			error = True

            # Significa que e impar
			filasErros.append(chave)
		else:
			error = False
	return filasErros, error


def buscamosRelacaoErradas(bitsFilasErradas):
	colunasRelacionadas = list()
	for indice, elementosFilas in enumerate(bitsFilasErradas):
		sentinela = False
		for bit in elementosFilas:
			try:
				int(bit)
				sentinela = True
			except:
				sentinela = False
				break
		if sentinela:
			colunasRelacionadas.append(indice)
	return colunasRelacionadas
    
def buscarColunasRelacionadas(filas, filasErradas):
	longitud = len(filas.values()[0])

	bitsFilasErradas = list()
	for i in range(longitud):
		bits = ""
		copyFilasErradas = list(filasErradas)
		while len(copyFilasErradas) > 0:
			filaObjetivo = copyFilasErradas.pop(0)
			for j in filasErradas:
				if j == filaObjetivo:
					bits += filas[j][i]
		bitsFilasErradas.append(bits)

	colunasRelacionadas = buscamosRelacaoErradas(bitsFilasErradas)
	return colunasRelacionadas


def quitarEspacosParidad(stringAuto):
	return stringAuto.replace("*", "")


def main():
    
    string_teste = "1010"
    print "String de entrada:", string_teste
    stringAuto = autocompletar(string_teste)
    originalStringAuto = stringAuto
    # print "-----------------"
    
    while True: 
		# print "String a analisar:", stringAuto
		filas = obterFilas(stringAuto)
		# print "valor de todas as filas:\n", filas

		(filasErradas, error) = buscarErros(filas)
		if not error:
			break
	
		# print "filas que continuam erradas:\n", filasErradas
	
    	# reparamos as filas erradas
		colunasRelacionadas = buscarColunasRelacionadas(filas, filasErradas)
		# print "columnas relacionadas:\n", colunasRelacionadas

		for i in colunasRelacionadas:
			copyStringAuto = originalStringAuto
			if stringAuto[i] == "0":
				print "1"
				stringAuto = copyStringAuto[:i] + "1" + copyStringAuto[i+1:]
				break
			else:
				print "2"
				stringAuto = copyStringAuto[:i] + "0" + copyStringAuto[i+1:]
				break

		# print "-----------------"
    
    # Excluir bits
    stringSemErros = quitarEspacosParidad(stringAuto)
    
    print "---RESULTADOS-----------------"
    print "ENTRADA", string_teste
    print "Saida ", stringSemErros
	


main()
